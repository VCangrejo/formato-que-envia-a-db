package formulario;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class ConectorDB {

    Connection miconexion; //clase para conectarnops//
    PreparedStatement clausula; //clase para crear clausulas sql//

    boolean conectar() {

        try {
            Class.forName("com.mysql.jdbc.Driver");

            miconexion = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/base1?autoReconnect=true&useSSL", "root", "mysql2019");
            //el driver manager es el encargado de crear la conexion
            System.out.println("me conecte a esta porqueria");
        } catch (Exception e) {
            System.out.println(e);
        }
        return true;
    }

    private float ph;
    private float temperatura;
    private String color;
    private String clima;
    private String cultivo;
    private float precipitacion;
    private int pendiente;
    private String orden;
    private String altura;
    private String usoactual; 
    
    
    public String getColor(){
        return color;
    }
    
    public void setColor (String color){
        this.color=color;
    }
    
    public float getPH(){
        return ph;
    }
    
    public void setPH(float PH){
        if(PH<0 || PH>14){
            this.ph=0;
        }else{
            this.ph=PH;
        }
    }
    public float getTemperatura(){
        return temperatura;
    }
    
    public void setTemperatura (float temperatura){
        this.temperatura=temperatura;
    }
    public String getClima(){
        return clima;
    }
    
    public void setTemperatura (String clima){
        this.clima=clima;
    }
    
     public String getCultivo(){
        return cultivo;
    }
    
    public void setCultivo (String cultivo){
        this.cultivo=cultivo;
    }
    
    public float getPrecipitacion(){
        return precipitacion;
    }
    
    public void setPrepicitacion (float precipitacion){
        this.precipitacion=precipitacion;
    }
     public int getPendiente(){
        return pendiente;
    }
    
    public void setPendiente(int Pendiente){
        if(Pendiente<0){
            this.pendiente=0;
        }else{
            this.pendiente=Pendiente;
        }
    }
     public String getOrden(){
        return orden;
    }
    
    public void setOrden (String orden){
        this.orden=orden;
    }
    public String getAltura(){
        return altura;
    }
    
    public void setAltura (String altura){
        this.altura=altura;
    }
    
     public String getUso(){
        return usoactual;
    }
    
    public void setUso (String usoactual){
        this.usoactual=usoactual;
    }
    
    /*
    
    boolean (String color) {

        int PH = 2;
        float temperatura = (float) 12.5;
        
        String humedad = "media";

        try {
            String insertar = "INSERT INTO `opciones` (`PH`, `temperatura`, `color`, `humedad`) VALUES (?,?,?,?)";
            clausula = miconexion.prepareStatement(insertar);
            clausula.setInt(,);
            clausula.setFloat(,);
            clausula.setString(,);
            clausula.setString(,;
            
            clausula.executeUpdate();
                 
        } catch (Exception e) {
            System.out.println(e);
        }

        return true;
    }
*/
}